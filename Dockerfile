FROM mcr.microsoft.com/dotnet/core/sdk:3.0
RUN mkdir /var/server \
    && chmod +rwx /var/server

COPY ./ServerApi /var/www/server/

WORKDIR /var/www/server

#RUN dotnet tool install --global --version 3.0.0 dotnet-ef \
#    && export PATH="$PATH:$HOME/.dotnet/tools/" \
#    && dotnet ef database update

RUN dotnet publish  --configuration=Release --output=/var/www/server/publish

WORKDIR /var/www/server/publish

ENV ASPNETCORE_URLS="http://+:80"

ENTRYPOINT ["dotnet","ServerApi.dll"]
