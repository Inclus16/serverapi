﻿
using Moq;
using Newtonsoft.Json;
using ServerApi;
using ServerApi.Models.Database;
using ServerApi.Models.Http.Requests;
using ServerApi.Services;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace Tests.Helpers
{
    class Authetificator
    {
        public static string GetJwt(SecurityProvider security, User user)
        {
            MethodInfo info = typeof(SecurityProvider).GetMethod("BuildToken", BindingFlags.NonPublic | BindingFlags.Instance);
            return typeof(SecurityProvider).GetMethod("BuildToken", BindingFlags.NonPublic | BindingFlags.Instance).Invoke( security,new object[] { user }).ToString();
        }
    }
}
