﻿
using Isopoh.Cryptography.Argon2;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServerApi.Models.Database;
using ServerApi.Services;
using ServerApi.Services.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tests.Helpers.Fixtures.Abstractions;

namespace Tests.Helpers
{
    public class FakeWebFactory<TStartup>
    : WebApplicationFactory<TStartup> where TStartup : class
    {
        public User User {  get; private set; }

        public static readonly string TEMP_PATH = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Storage"), "temp");


        public async  Task<DatabaseCase> InitNewDatabase(List<IFixture> fixtures=null)
        {
            DatabaseCase database = new DatabaseCase(Server);
            if (fixtures != null)
            {
                database.SetFixtures(fixtures);
            }
            return await database.FillDatabase();
        }

        public T GetService<T>()
        {
           IServiceScope sp= Services.CreateScope();
            return sp.ServiceProvider.GetRequiredService<T>();
        }


        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                    typeof(DbContextOptions<Postgres>));

                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }
                string postgresConnection = string.Empty;
                using (var sp = services.BuildServiceProvider())
                {
                    using (var scope = sp.CreateScope())
                    {
                        postgresConnection = Regex.Replace(scope.ServiceProvider.GetRequiredService<IConfiguration>().
                            GetConnectionString("Postgres"), @"Database=\w+;", "Database=" + Guid.NewGuid().ToString() + ";");
                    }
                }
                services.AddDbContext<Postgres>(options =>
                {
                    options.UseNpgsql(postgresConnection);
                });
                services.AddMemoryCache();
                services.AddScoped<UsersRepository>();
                services.AddScoped<CredentialsRepository>();
                services.AddScoped<MessagesRepository>();
                services.AddTransient<Validator>();
                services.AddTransient<SecurityProvider>();
                services.AddTransient<FileProvider>();
                services.AddTransient<DictionaryLoader>();
                // Build the service provider.
                var sp1 = services.BuildServiceProvider();

                // Create a scope to obtain a reference to the database
                // context (ApplicationDbContext).
                using (var scope = sp1.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var logger = scopedServices
                        .GetRequiredService<ILogger<FakeWebFactory<TStartup>>>();
                    scopedServices.GetRequiredService<Postgres>().Database.Migrate();

                }
            });
        }
    }
}
