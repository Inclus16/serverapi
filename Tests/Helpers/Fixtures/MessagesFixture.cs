﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using Tests.Helpers.Fixtures.Abstractions;
using Tests.Helpers.Fixtures.Helpers;

namespace Tests.Helpers.Fixtures
{
    class MessagesFixture : IFixture
    {
        private readonly List<Message> Data;

        public MessagesFixture()
        {
            Data = FixtureLoader.LoadRange<Message>();
        }
        public object GetData()
        {
            return Data;
        }

        public Type GetEntityType()
        {
            return typeof(Message);
        }

        public async Task Load(DbContext db)
        {
            await db.AddRangeAsync(Data);
            await db.SaveChangesAsync();
        }
    }
}
