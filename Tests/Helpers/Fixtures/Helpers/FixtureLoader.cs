﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Tests.Helpers.Fixtures.Helpers
{
    class FixtureLoader
    {
        private static readonly string FIXTURES_DATA_PATH = Path.Combine(Path.Combine(AppContext.BaseDirectory, "data"),"fixtures");

        public static List<T> LoadRange<T>()
        {
            string fixtureName = typeof(T).Name;
            return JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(Path.Combine(FIXTURES_DATA_PATH, fixtureName+".json")));
        }
    }
}
