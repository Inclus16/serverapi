﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Helpers.Fixtures.Abstractions
{
    public interface IFixture
    {
        Task Load(DbContext db);

        Type GetEntityType();

        object GetData();
    }
}
