﻿using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using ServerApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Helpers.Fixtures.Abstractions;
using Tests.Helpers.Fixtures.Helpers;

namespace Tests.Helpers.Fixtures
{
    class UsersFixture : IFixture
    {
        private readonly List<Credentials> Credentials;
        public UsersFixture()
        {
            Credentials = FixtureLoader.LoadRange<Credentials>();
        }


        public async Task Load(DbContext db)
        {
            List<Credentials> credentials = new List<Credentials>(Credentials);
            List<string> purePasswords = credentials.Select(x => x.Password).ToList();
            credentials.ForEach(x =>
            {
                x.Password = SecurityProvider.HashPassword(x.Password);
            });
            await db.AddRangeAsync(credentials);
            await db.SaveChangesAsync();
            for(int i = 0; i < purePasswords.Count; i++)
            {
                Credentials[i].Password = purePasswords[i];
            }
        }

        public Type GetEntityType()
        {
            return typeof(Credentials);
        }

        public object GetData()
        {
            return Credentials;
        }
    }
}
