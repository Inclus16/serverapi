﻿using Isopoh.Cryptography.Argon2;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ServerApi.Models.Database;
using ServerApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Helpers.Fixtures.Abstractions;

namespace Tests.Helpers
{
    public class DatabaseCase:IDisposable
    {
        public Postgres Db { get; private set; }

        private readonly IServiceScope Scope;

        private List<IFixture> Fixtures;

        public DatabaseCase(TestServer server)
        {
            Scope = server.Services.CreateScope();
            Postgres db = Scope.ServiceProvider.GetRequiredService<Postgres>();
            db.Database.EnsureDeleted();
            db.Database.Migrate();
            Db = db;
            FillDatabase();
        }

        public DatabaseCase SetFixtures(List<IFixture> fixtures)
        {
            Fixtures = fixtures;
            return this;
        }

        public List<T> GetFixtureData<T>()
        {
           return  Fixtures.Where(x => x.GetEntityType() == typeof(T)).First().GetData() as List<T>;
        }


        public async Task<DatabaseCase> FillDatabase()
        {
            if (Fixtures != null)
            {
               foreach(IFixture fixture in Fixtures)
                {
                  await fixture.Load(Db);
                }
            }
            return this;
        }
        public void Dispose()
        {
            ManagedDispose();
            GC.SuppressFinalize(this);

        }

        private void  ManagedDispose()
        {
            Db.Database.EnsureDeleted();
            Scope.Dispose();
        }

        ~ DatabaseCase()
        {
            ManagedDispose();
        }
    }
}
