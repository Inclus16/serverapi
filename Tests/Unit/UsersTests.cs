﻿using ServerApi;
using ServerApi.Models.Database;
using ServerApi.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Helpers;
using Tests.Helpers.Fixtures;
using Tests.Helpers.Fixtures.Abstractions;
using Xunit;

namespace Tests.Unit
{
    public class UsersTests : IClassFixture<FakeWebFactory<Startup>>
    {

        private readonly FakeWebFactory<Startup> Factory;
        public UsersTests(FakeWebFactory<Startup> factory)
        {
            Factory = factory;
        }

        [Fact]
        public async Task GetChatsTest()
        {
            using (DatabaseCase database = await Factory.InitNewDatabase(new List<IFixture> { new UsersFixture(),new MessagesFixture() }))
            {
                int requestedUserId = 1;
                int expectedChatUserId = 2;
                UsersRepository usersRepository = Factory.GetService<UsersRepository>();
                List<User> users = await usersRepository.GetChats(requestedUserId);
                Assert.True(users.Count == 1);
                Assert.Equal(expectedChatUserId, users.First().Id);
            }
        }
    }
}
