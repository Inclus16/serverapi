﻿using ServerApi;
using ServerApi.Models.Database;
using ServerApi.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Helpers;
using Tests.Helpers.Fixtures;
using Tests.Helpers.Fixtures.Abstractions;
using Xunit;

namespace Tests.Unit
{
    public class MessagesTests : IClassFixture<FakeWebFactory<Startup>>
    {
        private readonly FakeWebFactory<Startup> Factory;
        public MessagesTests(FakeWebFactory<Startup> factory)
        {
            Factory = factory;
        }

        [Fact]
        public async Task GetMessages()
        {
            using (DatabaseCase database = await Factory.InitNewDatabase(new List<IFixture> { new UsersFixture(), new MessagesFixture() }))
            {
                int requestedOwnerUserId = 1;
                int foreignUserId = 2;
                List<Message> messages = await Factory.GetService<MessagesRepository>().GetMessages(requestedOwnerUserId, foreignUserId);
                Assert.True(messages.Count == 2);
            }
        }
    }
}
