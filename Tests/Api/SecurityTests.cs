﻿
using Newtonsoft.Json;
using ServerApi;
using ServerApi.Models.Database;
using ServerApi.Models.Http.Requests;
using ServerApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tests.Helpers;
using Tests.Helpers.Fixtures;
using Xunit;

namespace Tests.Api
{
    public class SecurityTests : IClassFixture<FakeWebFactory<Startup>>
    {

        private readonly FakeWebFactory<Startup> Factory;
        public SecurityTests(FakeWebFactory<Startup> factory)
        {
            Factory = factory;
        }

        [Fact]
        public async Task TestVerifyUser()
        {
            HttpClient client = Factory.CreateClient();
           using (DatabaseCase database = await  Factory.InitNewDatabase(new List<Helpers.Fixtures.Abstractions.IFixture> { new UsersFixture() }))
            {
                Credentials credentials = database.GetFixtureData<Credentials>().First();
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(new LoginRequest
                {
                    Login = credentials.Login,
                    Password = credentials.Password
                }));
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await client.PostAsync("/api/access", httpContent);
                response.EnsureSuccessStatusCode();
                Assert.True(JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync())["token"].Length > 10);
            }
        }


        [Fact]
        public async Task RefreshJwt()
        {
            HttpClient client = Factory.CreateClient();
           using (DatabaseCase database = await Factory.InitNewDatabase(new List<Helpers.Fixtures.Abstractions.IFixture> { new UsersFixture() }))
            {
                Credentials credentials = database.GetFixtureData<Credentials>().First();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, "/api/access");
                request.Content = new StringContent(string.Empty);
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Authetificator.GetJwt(Factory.GetService<SecurityProvider>(), credentials.User));
                HttpResponseMessage response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                Assert.True(JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync())["token"].Length > 10);
           }
        }

        [Fact]
        public async Task Registrate()
        {
            HttpClient client = Factory.CreateClient();
            string newLogin = Guid.NewGuid().ToString();
            string newPassword = Guid.NewGuid().ToString();
            string newName = Guid.NewGuid().ToString();
            string newEmail = Guid.NewGuid().ToString() + "@mail.ru";
            using(DatabaseCase databaseCase = await Factory.InitNewDatabase())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/access/registrate");
                request.Content = new StringContent(JsonConvert.SerializeObject(new RegistrationRequest
                {
                    Login=newLogin,
                    Password=newPassword,
                    Name=newName,
                    Email=newEmail
                }));
                request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await  client.SendAsync(request);
                Assert.True(response.IsSuccessStatusCode);
                Assert.NotNull(databaseCase.Db.Users.FirstOrDefault(x => x.Email == newEmail&&x.Name==newName));
                Credentials credentials = databaseCase.Db.Credentials.FirstOrDefault(x => x.Login == newLogin);
                Assert.NotNull(credentials);
            }
        }
    }
}
