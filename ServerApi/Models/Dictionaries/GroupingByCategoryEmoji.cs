﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Dictionaries
{
    public class GroupingByCategoryEmoji
    {
        public string Category { get; set; }

        public List<Emoji> Emojis = new List<Emoji>();

        public int EmojisCount { get; set; }
    }
}
