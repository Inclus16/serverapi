﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Dictionaries
{
    public struct Emoji
    {
        public string Codes { get; set; }

        public string Char { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }
    }
}
