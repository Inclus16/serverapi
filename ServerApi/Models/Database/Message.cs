﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Database
{
    [Table("messages")]
    public class Message
    {
        [Column("id",TypeName ="SERIAL")]
        public int Id { get; set; }

        [Column("body",TypeName ="TEXT")]
        public string Body { get; set; }

        [Column("created_at",TypeName = "timestamp")]
        public DateTime CreatedAt { get; set; }

        [Column("sender_id",TypeName = "INT")]
        public int SenderId { get; set; }

        [Column("receiver_id", TypeName = "INT")]
        public int ReceiverId { get; set; }

        [ForeignKey("ReceiverId")]
        public User Receiver { get; set; }

        [ForeignKey("SenderId")]
        public User Sender { get; set; }

        public Message(string body, int sender,int receiver,DateTime createdAt)
        {
            SenderId = sender;
            Body = body;
            ReceiverId = receiver;
            CreatedAt = createdAt;
        }

        public Message() { }

        
    }
}
