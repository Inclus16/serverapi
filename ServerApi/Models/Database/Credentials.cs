﻿
using Microsoft.EntityFrameworkCore.Infrastructure;
using ServerApi.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Database
{
    [Table("credentials")]
    public class Credentials
    {
        [Column("id", TypeName = "SERIAL")]
        public int Id { get; set; }

        [Column("login", TypeName = "VARCHAR(100)")]
        public string Login { get; set; }

        [Column("password", TypeName = "VARCHAR(256)")]
        public string Password { get; set; }

        [Column("user_id", TypeName = "INT")]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        private User user;
        

        public User User
        {
            get => LazyLoader.Load(this, ref user);
            set => user = value;
        }

        public Credentials() { }

        private Credentials(ILazyLoader lazyLoader)
        {
            LazyLoader = lazyLoader;
        }

        private ILazyLoader LazyLoader { get; set; }


    }
}
