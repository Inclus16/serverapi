﻿using ServerApi.Models.Database;
using ServerApi.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Http.Requests
{
    public class RegistrationRequest
    {
        [Required(ErrorMessage = "Введите логин")]
        [StringLength(100,MinimumLength =6, ErrorMessage = "Максимальная длина логина - 100, минимальная -6")]
        [UniqueEntity("credentials", "login", ErrorMessage = "Такой логин уже используется")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [StringLength(4000, MinimumLength = 6, ErrorMessage = "Максимальная длина пароля - 4000, минимальная - 6")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Введите имя (ФИО)")]
        [StringLength(100, ErrorMessage = "Максимальная длина имени -100")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите Email адрес")]
        [StringLength(100, ErrorMessage = "Максимальная длина логина - 100")]
        [EmailAddress(ErrorMessage = "Ввидети существующий Email адрес")]
        [UniqueEntity("users","email",ErrorMessage = "Такой Email уже используется")]
        public string Email { get; set; }

        public Credentials GetModel()
        {
            return new Credentials
            {
                Login = Login,
                Password = Password,
                User = new User
                {
                    Name = Name,
                    Email = Email,
                    CreatedAt = DateTime.Now
                }
            };
        }
    }
}
