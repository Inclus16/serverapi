﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Models.Http.Requests
{
    public class MessageRequest
    {
        
        [Required(ErrorMessage = "Не указан получатель")]
        public int ReceiverId { get; set; }

        [Required(ErrorMessage = "Введи текст")]
        public string Body { get; set; }

        [Required]
        [DataType(DataType.DateTime,ErrorMessage = "Дата отправки сообщения не является действительной датой")]
        public string CreatedAt { get; set; }

        
    }
}
