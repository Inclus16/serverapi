﻿using Newtonsoft.Json;
using ServerApi.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServerApi.Services
{
    public class DictionaryLoader
    {
        private readonly FileProvider Files;
        public DictionaryLoader(FileProvider files)
        {
            Files = files;
        }
        public async Task LoadStaticDictionaries()
        {
            await LoadEmoji();
        }

        private async Task LoadEmoji()
        {
            HttpClient http = new HttpClient();
            HttpResponseMessage responseMessage = await http.GetAsync("https://unpkg.com/emoji.json@12.1.0/emoji.json");
            List<Emoji> emojis = JsonConvert.DeserializeObject<List<Emoji>>(await responseMessage.Content.ReadAsStringAsync());
            List<Emoji> favoriteEmojis = new List<Emoji>();
            List<string> savedNames = new List<string>();
            int count = emojis.Count;
            for(int i = 0; i < count; i++)
            {
                if(!emojis[i].Category.Contains("Smileys & Emotion"))
                {
                    break;
                }
                if (savedNames.Contains(emojis[i].Name))
                {
                    continue;
                }
                favoriteEmojis.Add(emojis[i]);
                savedNames.Add(emojis[i].Name);
            }
            await Files.SaveFile(JsonConvert.SerializeObject(favoriteEmojis), "dictionaries", "emojies.json");
        }
    }
}
