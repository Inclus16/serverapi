﻿using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services
{
    public class Postgres : DbContext
    {
        public Postgres(DbContextOptions<Postgres> options)
        : base(options)
        { }

        public DbSet<Credentials> Credentials { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Message>().HasOne<User>(x => x.Receiver);
            builder.Entity<Message>().HasOne<User>(x => x.Sender);
        }


    }
}
