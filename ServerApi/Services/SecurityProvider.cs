﻿using Isopoh.Cryptography.Argon2;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ServerApi.Models.Database;
using ServerApi.Models.Http.Requests;
using ServerApi.Services.Repositories;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace ServerApi.Services
{
    public class SecurityProvider
    {

        private readonly IConfiguration Configuration;

        private readonly CredentialsRepository Credentials;

        private readonly UsersRepository Users;
        public SecurityProvider(
             CredentialsRepository credentials,
             UsersRepository users,
             IConfiguration configuration)
        {
            Configuration = configuration;
            Users = users;
            Credentials = credentials;
        }


        public async Task<string> GetToken(LoginRequest request)
        {
            Credentials credentials = await Credentials.GetByLogin(request.Login);
            if (credentials == null || !VerifyUser(credentials.Password, request.Password))
            {
                return null;
            }
            return BuildToken(credentials.User);
        }

        public static int GetUserId(ClaimsPrincipal claims)
        {
            return Convert.ToInt32(claims.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).First().Value);
        }

       public static bool VerifyUser(string dbPassword, string providedPassword)
        {
            return Argon2.Verify(dbPassword, providedPassword);
        }

        public static string HashPassword(string password)
        {
            return Argon2.Hash(password);
        }

        public async Task Registrate(Credentials credentials)
        {
            credentials.Password = HashPassword(credentials.Password);
            await  Credentials.Insert(credentials);
        }

        private string BuildToken(User user)
        {
            DateTime now = DateTime.Now;
            JwtSecurityToken jwt = new JwtSecurityToken(
                  notBefore: now,
                  claims: GetIdentity(user).Claims,
                  issuer: Configuration.GetValue<string>("JwtIssuer"),
                  expires: now.Add(TimeSpan.FromMinutes(Configuration.GetValue<double>("JwtLifetime"))),
                  signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetValue<string>("JwtToken"))), SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Name),
                    new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                    new Claim(ClaimTypes.Email,user.Email)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;

        }

    

        public async Task<string> RefreshToken(string token)
        {
            ClaimsPrincipal claims = GetPrincipalFromExpiredToken(token);
            User user =  await Users.GetUser(Convert.ToInt32(claims.FindFirst(ClaimTypes.NameIdentifier).Value));
            if (user == null)
            {
                throw new SecurityTokenException("User blocked or deleted");
            }
            return BuildToken(user);

        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetValue<string>("JwtToken"))),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }

            return principal;
        }

    }
}
