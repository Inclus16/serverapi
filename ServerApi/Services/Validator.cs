﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services
{
    public class Validator
    {
        private object Model;

        public bool IsModelValid { get; private set; }

        public List<ValidationResult> Errors { get; private set; }

        public void SetModelToValidate(object model)
        {
            Model = model;
        }

        public void Validate()
        {
            Errors = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(Model);
            IsModelValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(Model, context, Errors);
        }

    }
}
