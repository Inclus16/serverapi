﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ServerApi.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services.Repositories
{
    public class MessagesRepository:AbstractRepository<Message>
    {
        public MessagesRepository(Postgres db) : base(db) { }
        public async Task<List<Message>> GetMessages(int userId,int foreignUserId)
        {
            return await Db.Messages.Where(x => (x.ReceiverId == userId && x.SenderId == foreignUserId)
                                || (x.ReceiverId == foreignUserId && x.SenderId == userId)).Include(x => x.Receiver).Include(x => x.Sender).AsNoTracking().ToListAsync();
        }
        public new async Task Insert(Message message)
        {
            EntityEntry entry = await Db.Messages.AddAsync(message);
            await entry.Reference("Receiver").LoadAsync();
            await entry.Reference("Sender").LoadAsync();
            await Db.SaveChangesAsync();
        }

    }
}
