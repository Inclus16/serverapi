﻿using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services.Repositories
{
    public class CredentialsRepository:AbstractRepository<Credentials>
    {
        public CredentialsRepository(Postgres db):base(db)
        {

        }

        public async Task<Credentials> GetByLogin(string login)
        {
           
            return await Db.Credentials.FirstOrDefaultAsync(x => x.Login == login);
        }
    }
}
