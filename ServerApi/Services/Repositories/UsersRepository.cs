﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using ServerApi.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services.Repositories
{
    public class UsersRepository:AbstractRepository<User>
    {

        public UsersRepository(Postgres db) : base(db)
        {
        }

        public async Task<List<User>> GetUsers()
        {
            return await Db.Users.ToListAsync();
        }

        public List<User> GetUsers(Func<User, bool> predicate)
        {
            return Db.Users.AsNoTracking().Where(predicate).ToList();
        }

        public async Task<User> GetUser(int id)
        {
            return await Db.Users.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }


        public async Task<List<User>> GetChats(int userId)
        {
            List<User> users = new List<User>();
            NpgsqlConnection connection = Db.Database.GetDbConnection() as NpgsqlConnection;
            return await Db.Users.FromSqlInterpolated($"SELECT users.* FROM public.messages JOIN users ON users.id=messages.receiver_id OR users.id=messages.sender_id WHERE(messages.receiver_id ={userId} OR messages.sender_id = {userId} ) AND users.id != {userId}  GROUP BY users.id  ").ToListAsync();
           
        }
    }
}
