﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services.Repositories
{
    public abstract class AbstractRepository<T>
    {
        protected readonly Postgres Db;

        public AbstractRepository(Postgres db)
        {
            Db = db;
        }

        public async Task Insert(T model)
        {
           await Db.AddAsync(model);
           await Db.SaveChangesAsync();
        }

        public async Task Insert(List<T> models)
        {
            await Db.AddRangeAsync(models);
            await Db.SaveChangesAsync();
        }

        public async Task Delete(T model)
        {
            Db.Remove(model);
            await Db.SaveChangesAsync();
        }

        public async Task Delete(List<T> model)
        {
            Db.RemoveRange(model);
            await Db.SaveChangesAsync();
        }

        public async Task Update(T model)
        {
            Db.Update(model);
            await Db.SaveChangesAsync();
        }

        public async Task Update(List<T> models)
        {
            Db.UpdateRange(models);
            await Db.SaveChangesAsync();
        }

    }
}
