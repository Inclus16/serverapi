﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Services
{
    public class FileProvider
    {
        private readonly string WebRootPath = Path.Combine(AppContext.BaseDirectory, "wwwroot");

        public async Task SaveFile(string data, string path, string fileName)
        {
            await SaveTextToFile(data, path, fileName);
        }

        public async Task SaveFile(Stream stream,string path,string fileName)
        {
            StreamReader reader = new StreamReader(stream);
            await SaveTextToFile(await reader.ReadToEndAsync(), path, fileName);
        }

        private async Task SaveTextToFile(string data,string path,string fileName)
        {
            string absolutePath = Path.Combine(WebRootPath, path);
            Directory.CreateDirectory(absolutePath);
            await  File.WriteAllTextAsync(Path.Combine(absolutePath,fileName), data);
        }
    }
}
