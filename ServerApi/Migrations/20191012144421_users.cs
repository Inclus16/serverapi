﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ServerApi.Migrations
{
    public partial class users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(type: "SERIAL"),
                    name = table.Column<string>(type: "VARCHAR(100)"),
                    email = table.Column<string>(type: "VARCHAR(100)"),
                    created_at = table.Column<DateTime>(type: "TIMESTAMP WITHOUT TIME ZONE",nullable:true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                    
                });

            migrationBuilder.CreateTable(
                name: "credentials",
                columns: table => new
                {
                    id = table.Column<int>(type: "SERIAL"),
                    login = table.Column<string>(type: "VARCHAR(100)"),
                    password = table.Column<string>(type: "VARCHAR(256)"),
                    user_id = table.Column<int>(type: "INT")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_credentials", x => x.id);
                    table.ForeignKey(
                        name: "FK_credentials_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_credentials_user_id",
                table: "credentials",
                column: "user_id");
            migrationBuilder.AddUniqueConstraint("UQ_users_email", "users", "email");
            migrationBuilder.AddUniqueConstraint("UQ_credentials_login", "credentials", "login");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "credentials");

            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
