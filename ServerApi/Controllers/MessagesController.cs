﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using ServerApi.Services;
using ServerApi.Services.Repositories;

namespace ServerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessagesController : ControllerBase
    {
        private readonly MessagesRepository Messages;
        public MessagesController(MessagesRepository messages)
        {
            Messages = messages;
        }

        [HttpGet("{id:int}")]
        public async Task<List<Message>> Get(int id)
        {
            int userId = SecurityProvider.GetUserId(User);
            return await Messages.GetMessages(userId,id);
        }
    }
}