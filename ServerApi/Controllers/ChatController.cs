﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServerApi.Models.Database;
using ServerApi.Services;
using ServerApi.Services.Repositories;

namespace ServerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChatController : ControllerBase
    {
        private readonly UsersRepository Users;

        public ChatController(UsersRepository users)
        {
            Users = users;
        }

        [HttpGet]
        public async Task<List<User>> Get()
        {
            int userId = SecurityProvider.GetUserId(User);
            return await Users.GetChats(userId);
        }
    }
}