﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerApi.Models.Database;
using ServerApi.Services;
using ServerApi.Services.Repositories;

namespace ServerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly UsersRepository Users;
        public UsersController(UsersRepository users)
        {
            Users = users;
        }

        [HttpGet]
        public ActionResult<User[]> Get()
        {
            return Ok(Users.GetUsers(x=>x.Id!=SecurityProvider.GetUserId(User)));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            User user = await Users.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
    }
}