﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerApi.Models.Http.Requests;
using ServerApi.Services;

namespace ServerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        private readonly SecurityProvider Security;
        public AccessController(SecurityProvider security)
        {
            Security = security;
        }

        [HttpPost]
        public async Task<IActionResult> Post(LoginRequest request)
        {
            string jwt = await Security.GetToken(request);
            if (jwt == null)
            {
                return Forbid();
            }
            return Ok(new { token = jwt });
        }

        [HttpPost("registrate")]
        public async  Task<IActionResult> Registrate(RegistrationRequest request)
        {
            await  Security.Registrate(request.GetModel());
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromHeader(Name = "Authorization")] string authorization)
        {
            try
            {
                string jwt = await Security.RefreshToken(authorization.Replace("Bearer ", string.Empty));
                return Ok(new { token = jwt });
            }
            catch (Exception)
            {
                return Forbid();
            }
        }
    }
}