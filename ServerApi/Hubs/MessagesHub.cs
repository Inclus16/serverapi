﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ServerApi.Models.Database;
using ServerApi.Models.Http.Requests;
using ServerApi.Services;
using ServerApi.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApi.Hubs
{
    [Authorize]
    public class MessagesHub : Hub
    {
        private readonly Postgres Db;

        private readonly Validator Validator;

        private readonly MessagesRepository Messages;

        public MessagesHub(Postgres db,
                                          Validator validator,
                                          MessagesRepository messages)
        {
            Db = db;
            Validator = validator;
            Messages = messages;
        }

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public async Task Send(MessageRequest request)
        {
            Validator.SetModelToValidate(request);
            Validator.Validate();
            if (!Validator.IsModelValid)
            {
                await Clients.Caller.SendAsync("Error", Validator.Errors.First().ErrorMessage);
                return;
            }
            int senderId = Convert.ToInt32(Context.UserIdentifier);
            Message message = new Message(
                receiver: request.ReceiverId,
                sender: senderId,
                body: request.Body,
                createdAt: DateTime.ParseExact(request.CreatedAt, "dd-MM-yyyy HH:mm:ss", null)
                );
            await  Messages.Insert(message);
            await Clients.User(request.ReceiverId.ToString()).SendAsync("Receive", message);
        }
    }
}
